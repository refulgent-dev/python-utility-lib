class NotParseableException(Exception):

    def __init__(self, node, message):

        self.__debugLevel = 0

        if self.__debugLevel > 0:
            print('NotParseableException: Test expression is not parseable!')
        
            testExpression = ''
            if node._nodes:
                for n in node._nodes:
                    if type(n) is object:
                        testExpression += '%s ' % n._value
                    if type(n) is tuple:
                        testExpression += '%s ' % n[1]


            print('%s -> %s' % (message,testExpression))
            node.printNodeList()