import re

# Tokentypes
#
# ch:
#   String of characters of unknown composition
#
# es:
#   Escaped character
#
# op:
#   Operator character
#
# va:
#   String containing parameter
#
class Tokenizer():

    def __init__(self, testString):

        Tokenizer.PATTERN_ESCAPED = re.compile(r"(?:\\.)|(?:[^\\]+)")
        Tokenizer.PATTERN_OPERATOR = re.compile(r"[&\|!\(\)]|[^&\|!\(\)]+")

        self.__tokenList = list()

        # process escaped characters
        for fragment in Tokenizer.PATTERN_ESCAPED.findall(testString):
            if fragment[0] == '\\':
                self.__tokenList.append(('es', fragment))
            else:
                self.__tokenList.append(('ch', fragment))

        if len(self.__tokenList) == 0:
            self.__tokenList.append(('ch', ''))

    def __fuseTokens(self, tokens):

        value = ''
        for token in tokens:

            if token[0] != 'ch':
                raise Exception('Tokenizer: Cannot fuse non-string token!')

            value += token[1]

        return value.strip()

    def tokenize(self):

        # split at operators
        tokenList = list()
        for token in self.__tokenList:

            if token[0] == 'es':
                tokenList.append(('ch',token[1][1]))

            if token[0] == 'ch':
                for fragment in Tokenizer.PATTERN_OPERATOR.findall(token[1]):
                    if fragment in ['&','|','!','(',')']:
                        tokenList.append(('op', fragment))
                    else:
                        tokenList.append(('ch', fragment))

        self.__tokenList = tokenList

        # fusing string tokens
        tokenList = list()
        tokenBuffer = list()

        for token in self.__tokenList:

            if token[0] == 'op':

                if len(tokenBuffer) > 0:
                    value = self.__fuseTokens(tokenBuffer)
                    if value != '':
                        tokenList.append(('va',value))
                        tokenBuffer = list()

                tokenList.append(token)
            
            if token[0] == 'ch':
                tokenBuffer.append(token)
            
        if len(tokenBuffer) > 0:
                    value = self.__fuseTokens(tokenBuffer)
                    if value != '':
                        tokenList.append(('va',value))
                        tokenBuffer = list()

        if len(tokenList) == 0:
            tokenList.append(('va',''))

        self.__tokenList = tokenList

        return self.__tokenList

if __name__ == '__main__':
    print(Tokenizer('!!(a)').tokenize())