import unittest
import multiprocessing

from constructa.util.test.classes.concurrent_test_suite import ConcurrentTestSuite

from constructa.util.test import StringSetEvaluatorChunkTest

if __name__ == '__main__':

    numProcesses = multiprocessing.cpu_count()**2

    suite = ConcurrentTestSuite()

    testCases = []
    for index in range(numProcesses):
        suite.addTest(StringSetEvaluatorChunkTest(
                'test_chunk', 
                numSymbols = 3,
                numChunks = numProcesses,
                chunkIndex = index))
  
    unittest.TextTestRunner(verbosity=2).run(suite)