import unittest
import random
import re
import os

from constructa.util import StringSetEvaluator
from constructa.util.classes import NotParseableException
from constructa.util.test.classes.pattern_iterator import PatternIterator
from constructa.util.test.classes.selection_iterator import SelectionIterator
from constructa.util.test.classes.query_iterator import QueryIterator
from constructa.util.test.classes.pattern_query_test import PatternQueryTest

class StringSetEvaluatorChunkTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        self.__numSymbols = kwargs['numSymbols']
        self.__numChunks = kwargs['numChunks']
        self.__chunkIndex = kwargs['chunkIndex']

        kwargs = {}
        super().__init__(*args, **kwargs)


    def test_chunk(self):

        iterations = 0

        for selectionIterator in PatternIterator(self.__numSymbols, self.__numChunks, self.__chunkIndex):
            for queryIterator in selectionIterator:
                for test in queryIterator:

                    value = test.performEvaluation()

                    if value == None:
                        with self.assertRaises(NotParseableException, msg="#%i test=>%s<" % (iterations, selectionIterator.getParameters())):
                            test.performTest()

                    try:    
                        if value == True:
                            self.assertTrue(test.performTest(), msg="#%i test=>%s<" % (iterations, selectionIterator.getParameters()))
                    except Exception:
                        raise AssertionError("#%i test=>%s<" % (iterations, selectionIterator.getParameters()))

                    try:
                        if value == False:
                            self.assertFalse(test.performTest(), msg="#%i test=>%s<" % (iterations, selectionIterator.getParameters()))
                    except Exception:
                        raise AssertionError("#%i test=>%s<" % (iterations, selectionIterator.getParameters()))
                        
                    iterations += 1

        print("Task %i performed %i iterations" % (self.__chunkIndex, iterations))


if __name__ == '__main__':
    unittest.main()


