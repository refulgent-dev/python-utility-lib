import math, itertools

from test_iterator import TestIterator

class ParameterIterator(TestIterator):
    
    PARAM_LIST = [
                '',
                'a',
                'bc',
                ' de',
                'fg ',
                ' hi ']
    #            'jkl',
    #            ' mno',
    #            'pqr ',
    #            ' stu ']



    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if len(args) != 1:
            raise Exception("Incorrect number of parameters (need: numParameters)")

        self.__numParams = args[0]


        # Variation mit Wiederholung
        n = len(ParameterIterator.PARAM_LIST)
        k = self.__numParams
        self._numIterations = math.comb(n+k-1, n-1)

        if self._numIterations == 0:
            self._numIterations = 1


        #combinations_with_replacement(ParameterIterator.PARAM_LIST, r=self.__numParams)
        


        
    
    