from test_iterator import TestIterator
from pattern_iterator import PatternIterator
from parameter_iterator import ParameterIterator
from operator_iterator import OperatorIterator
from query_iterator import QueryIterator

class DimensionIterator(TestIterator):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Metrik
        MAX_NUM_PARAMS = 4
        MAX_NUM_OPS = 9
        
        self.__numChunks = kwargs["numChunks"]
        self.__chunkIndex = kwargs["chunkIndex"]

        self.__iterationsDistributionTable = list()

        # get total # iterations, iterations distribution
        for pIndex in range(MAX_NUM_PARAMS+1):
            for oIndex in range(MAX_NUM_OPS+1):

                self._linkIterator(     PatternIterator(pIndex,oIndex, next=
                                        ParameterIterator(pIndex, next=
                                        OperatorIterator(oIndex, next=
                                        QueryIterator(pIndex)))))

                subIteratorTotalIterations = self._successor.getTotalIterations()

                self.__iterationsDistributionTable.append(subIteratorTotalIterations)
                self._numIterations += subIteratorTotalIterations

                #print("pa=%i op=%i ti=%i" % (Pa, Op, ti))

        self._firstIteration = self.__chunkIndex * self._numIterations / self.__numChunks #
        self._lastIteration = (self.__chunkIndex + 1) * self._numIterations / self.__numChunks - 1 #

        pIt = 0
        cIt = 0
        for ti in self.__iterationsDistributionTable:

            if (ti + cIt) >= (self._firstIteration - 2) and self._firstSubIteratorIndex == -1:
                self._firstSubIteratorIndex = pIt
                self._firstSubIteratorFirstIteration = ti + cIt - (self._firstIteration - 2)

                if self.__chunkIndex == 0 and pIt == 0:
                    self._firstSubIteratorFirstIteration = 0
                #print("it=%i first=%i" % (pIt, self.__iteratorTable[pIt]._firstIteration))

            if (ti + cIt) >= self._lastIteration and self._lastSubIteratorIndex == -1:
                self._lastSubIteratorIndex = pIt
                self._lastSubIteratorLastIteration = ti + cIt - self._lastIteration
                #print("it=%i last=%i" % (pIt, self.__iteratorTable[pIt]._lastIteration))

            #print("iterator=%i first=%i last=%i ti=%i ci=%i" % (pIt, self._firstSubIteratorFirstIteration , self._lastSubIteratorLastIteration, ti, cIt))

            pIt += 1
            cIt += ti
        
        #print("f=%i l=%i" % (self._successor.calculateFirstIteration(), self._successor.calculateLastIteration()))
        #print("first=%i last=%i total=%i ci=%i" % (self.__firstIteratorIndex, self.__lastIteratorIndex , len(self.__iteratorTable), self._lastIteration - self._firstIteration))


    # chunk iterator...
    def __iter__(self):
        return self

    def __next__(self):
        pass

    def getTotalIterations(self):
        return self._numIterations

        