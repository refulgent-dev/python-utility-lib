from math import factorial

import combi

from test_iterator import TestIterator


class PatternIterator(TestIterator):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if len(args) != 2:
            raise Exception("Incorrect number of parameters (need: numParameters, numOperators)")

        self.__numParameters = args[0]
        self.__numOperators = args[1]

        patternList = list()

        for i in range(self.__numParameters):
            patternList.append('P')
        for i in range(self.__numOperators):
            patternList.append('O')

        patternSpace = combi.PermSpace(patternList)

        # Permutation ohne Wiederholung
        self._numIterations = len(patternSpace)
