from test_iterator import TestIterator
import math

class OperatorIterator(TestIterator):

    OPERATOR_LIST = ['&','|','!','(',')']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if len(args) != 1:
            raise Exception("Incorrect number of parameters (need: numOperators)")

        self.__numOperators = args[0]

        # Variation mit Wiederholung
        n = len(OperatorIterator.OPERATOR_LIST)
        k = self.__numOperators
        self._numIterations = math.comb(n+k-1, n-1)

        if self._numIterations == 0:
            self._numIterations = 1

        #combinations_with_replacement(OperatorIterator.OPERATOR_LIST, r=self.__numOperators)