from math import comb
import itertools

from test_iterator import TestIterator
from parameter_iterator import ParameterIterator

class QueryIterator(TestIterator):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if len(args) != 1:
            raise Exception("Incorrect number of parameters (need: numParameters)")

        self.__numParameters = args[0]

        #Kombination ohne Wiederholung
        self._numIterations = 0
        for params in range(self.__numParameters+1):
            self._numIterations += comb(len(ParameterIterator.PARAM_LIST), params)

        if self._numIterations == 0:
            self._numIterations = 1

    def __iter__(self):

        self.__params = 0
        self.__combinator = itertools.combinations(ParameterIterator.PARAM_LIST, r=self.__params)
        return self

    def __next__(self):
        
        nextValue = next(self.__combinator, None)

        if nextValue is None:
            if self.__params == self.__numParameters:
                raise StopIteration

            self.__params += 1
            self.__combinator = itertools.combinations(ParameterIterator.PARAM_LIST, r=self.__params)
            nextValue = self.__next__()
            
        return nextValue
    