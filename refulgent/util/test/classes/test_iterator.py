class TestIterator:

    def __init__(self, *args, **kwargs):
        
        if "next" in kwargs:
            self._linkIterator(kwargs["next"])  
        else:
            self._successor = None

        ###

        self._numIterations = 1
        self._numNodeIterations = 1 # self and subiterators

        self._firstIteration = 0
        self._lastIteration = -1

        self._firstSubIteratorIndex = -1
        self._lastSubIteratorIndex = -1 
        self._firstSubIteratorFirstIteration = 0
        self._lastSubIteratorLastIteration = -1

        ###

    def _linkIterator(self, successor):
        self._successor = successor
        self._successor._predecessor = self

    # ist sparsam da schon in den Konstruktoren gerechnet wurde
    def getTotalIterations(self):
        
        if self._successor:
            self._numNodeIterations = self._successor.getTotalIterations() * self._numIterations
            return self._numNodeIterations
        else:
            return self._numIterations

      # ist sparsam da schon in den Konstruktoren gerechnet wurde
    def getIteratorChain(self):
        
        if self._successor:
            
            return [self] + self._successor.getIteratorChain()
        else:
            return [self]


    def calculateFirstIteratorIndex(self):
        return self._predecessor._firstSubIteratorFirstIteration // self._successor._numNodeIterations

    def calculateFirstIteration(self):
        return self._predecessor._firstSubIteratorFirstIteration % self._successor._numNodeIterations

    def calculateLastIteratorIndex(self):        
        return self._predecessor._lastSubIteratorLastIteration // self._successor._numNodeIterations

    def calculateLastIteration(self):        
        return self._predecessor._lastSubIteratorLastIteration % self._successor._numNodeIterations
