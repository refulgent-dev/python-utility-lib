from constructa.util import StringSetEvaluator
from constructa.util.classes import NotParseableException

import unittest

# Basic test cases for StringSetEvaluator
#
class StringSetEvaluatorStaticTest(unittest.TestCase):

    def test_static_const(self):

#        evaluator = StringSetEvaluator('a &     (    !b | c) | (d&(f|h))')

#        self.assertTrue(evaluator.evaluate(['a']))
#        self.assertFalse(evaluator.evaluate(['a']))
#        self.assertEqual('foo'.upper(), 'FOO')
#        try:
#            with self.assertRaises(NotParseableException):
#                evaluator = StringSetEvaluator('a&!')
#        except (NotParseableException):
#            pass


        try:
            with self.assertRaises(TypeError):
                evaluator = StringSetEvaluator()
        except (TypeError):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator([])
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(None)
        except (NotParseableException):
            pass

    
        emptyEvaluator = StringSetEvaluator('')
        self.assertIsNotNone = (emptyEvaluator)

        

        self.assertTrue(emptyEvaluator.evaluate([]))
        self.assertTrue(emptyEvaluator.evaluate(['']))


        self.assertTrue(emptyEvaluator.evaluate(['','a']))
        self.assertTrue(emptyEvaluator.evaluate(['a','']))
        self.assertFalse(emptyEvaluator.evaluate(['a']))

        constantEvaluator = StringSetEvaluator('a')

        self.assertFalse(constantEvaluator.evaluate(['']))
        self.assertTrue(constantEvaluator.evaluate(['a']))
        self.assertFalse(constantEvaluator.evaluate(['b']))

        self.assertTrue(constantEvaluator.evaluate(['','a']))
        self.assertTrue(constantEvaluator.evaluate(['a','']))

        self.assertFalse(constantEvaluator.evaluate(['','b']))
        self.assertFalse(constantEvaluator.evaluate(['b','']))

        self.assertTrue(constantEvaluator.evaluate(['a','b']))
        self.assertTrue(constantEvaluator.evaluate(['b','a']))

        negatedConstantEvaluator = StringSetEvaluator('!a')

        self.assertTrue(negatedConstantEvaluator.evaluate(['']))
        self.assertFalse(negatedConstantEvaluator.evaluate(['a']))
        self.assertTrue(negatedConstantEvaluator.evaluate(['b']))

        self.assertFalse(negatedConstantEvaluator.evaluate(['','a']))
        self.assertFalse(negatedConstantEvaluator.evaluate(['a','']))

        self.assertTrue(negatedConstantEvaluator.evaluate(['','b']))
        self.assertTrue(negatedConstantEvaluator.evaluate(['b','']))

        self.assertFalse(negatedConstantEvaluator.evaluate(['a','b']))
        self.assertFalse(negatedConstantEvaluator.evaluate(['b','a']))


        parenthesesConstantEvaluator = StringSetEvaluator('(a)')

        self.assertFalse(parenthesesConstantEvaluator.evaluate(['']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['a']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['b']))

        self.assertTrue(parenthesesConstantEvaluator.evaluate(['','a']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['a','']))

        self.assertFalse(parenthesesConstantEvaluator.evaluate(['','b']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['b','']))

        self.assertTrue(parenthesesConstantEvaluator.evaluate(['a','b']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['b','a']))


    def test_static_neg(self):


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!')
        except (NotParseableException):
            pass

        parenthesesConstantEvaluator = StringSetEvaluator('(!a)')

        self.assertTrue(parenthesesConstantEvaluator.evaluate(['']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['a']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['b']))

        self.assertFalse(parenthesesConstantEvaluator.evaluate(['','a']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['a','']))

        self.assertTrue(parenthesesConstantEvaluator.evaluate(['','b']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['b','']))

        self.assertFalse(parenthesesConstantEvaluator.evaluate(['a','b']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['b','a']))


        parenthesesConstantEvaluator = StringSetEvaluator('!(a)')

        self.assertTrue(parenthesesConstantEvaluator.evaluate(['']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['a']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['b']))

        self.assertFalse(parenthesesConstantEvaluator.evaluate(['','a']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['a','']))

        self.assertTrue(parenthesesConstantEvaluator.evaluate(['','b']))
        self.assertTrue(parenthesesConstantEvaluator.evaluate(['b','']))

        self.assertFalse(parenthesesConstantEvaluator.evaluate(['a','b']))
        self.assertFalse(parenthesesConstantEvaluator.evaluate(['b','a']))
        


    def test_static_and(self):
        
        testStrings = [ 'a&b',
                        'a&!b'  
                        'a&(b)',
                        'a&(!b)',
                        'a&!(b)',
                        '!a&b',
                        '!a&!b',
                        '!a&(b)',
                        '!a&(!b)',
                        '!a&!(b)',
                        '(a)&b',
                        '(a)&!b',
                        '(a)&(b)',
                        '(a)&(!b)',
                        '(a)&!(b)',
                        '(!a)&b',
                        '(!a)&!b',
                        '(!a)&(b)',
                        '(!a)&(!b)',
                        '(!a)&!(b)']

        for ts in testStrings:
            self.assertIsNotNone(StringSetEvaluator(ts), msg='test=>%s<' % ts)
        

        testStrings = [
            '!',
            'a&b!',
            'a& ',
            'a&! ',
            'a& !',
            'a&( )',
            'a&(! )',
            'a&!( )',
            '!a&b!',
            '!a& ',
            '!a&! ',
            '!a& !',
            '!a&( )',
            '!a&(! )',
            '!a&!( )',
            '(a)&b!',
            '(a)& ',
            '(a)&! ',
            '(a)& !',
            '(a)&( )',
            '(a)&(! )',
            '(a)&!( )',
            '(!a)&b!',
            '(!a)& ',
            '(!a)&! ',
            '(!a)& !',
            '(!a)&( )',
            '(!a)&(! )',
            '(!a)&!( )',
            'a!&b',
            'a!&!b', 
            'a!&(b)',
            'a!&(!b)',
            'a!&!(b)',
            'a!&b!',
            'a!& ',
            'a!&! ',
            'a!& !',
            'a!&( )',
            'a!&(! )',
            'a!&!( )',
            ' &b',
            ' &!b',
            ' &(b)',
            ' &(!b)',
            ' &!(b)',
            ' &b!',
            ' & ',
            ' &! ',
            ' & !',
            ' &( )',
            ' &(! )',
            ' &!( )',
            '! &b',
            '! &!b',
            '! &(b)',
            '! &(!b)',
            '! &!(b)',       
            '! &b!',
            '! & ',
            '! &! ',
            '! & !',
            '! &( )',
            '! &(! )',
            '! &!( )',
            ' !&b',
            ' !&!b',
            ' !&(b)',
            ' !&(!b)',
            ' !&!(b)',
            ' !&b!',
            ' !& ',
            ' !&! ',
            ' !& !',
            ' !&( )',
            ' !&(! )',
            ' !&!( )',
            '( )&b',
            '( )&!b',
            '( )&(b)',
            '( )&(!b)',
            '( )&!(b)',
            '( )&b!',
            '( )& ',
            '( )&! ',
            '( )& !',
            '( )&( )',
            '( )&(! )',
            '( )&!( )',
            '(! )&b',
            '(! )&!b',
            '(! )&(b)',
            '(! )&(!b)',
            '(! )&!(b)',
            '(! )&b!',
            '(! )& ',
            '(! )&! ',
            '(! )& !',
            '(! )&( )',
            '(! )&(! )',
            '(! )&!( )',
            '!( )&b',
            '!( )&!b',
            '!( )&(b)',
            '!( )&(!b)',
            '!( )&!(b)',
            '!( )&b!',
            '!( )& ',
            '!( )&! ',
            '!( )& !',
            '!( )&( )',
            '!( )&(! )',
            '!( )&!( )']

        for ts in testStrings:
            try:
                with self.assertRaises(NotParseableException, msg=('test=>%s<' % ts)):
                    StringSetEvaluator(ts)
            except (NotParseableException):
                pass


    def test_static_or(self):
        
        testStrings = [
                'a|b',
                'a|!b',
                'a|(b)',
                'a|(!b)',

                '!a|b',
                '!a|!b',
                '!a|(b)',
                '!a|(!b)',

                '(a)|b',
                '(a)|!b',
                '(a)|(b)',
                '(a)|(!b)',

                '(!a)|b',
                '(!a)|!b',
                '(!a)|(b)',
                '(!a)|(!b)',
                '(!a)|!(b)']

        for ts in testStrings:
            self.assertIsNotNone(StringSetEvaluator(ts), msg='test=>%s<' % ts)

        #-
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a|b!')
        except (NotParseableException):
            pass

        #TODO not failing
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a| ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a|!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a| ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!a|!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a)|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a)| ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a)|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a)| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(a)|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a)|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a)|!( )')
        except (NotParseableException):
            pass
        
        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(!a)|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(!a)| ')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(!a)|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(!a)| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(!a)|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('(!a)|(! )')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('(!a)|!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|b')
        except (NotParseableException):
            pass
    
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|!b') 
        except (NotParseableException):
            pass
       
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|(b)')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|(!b)')
        except (NotParseableException):
            pass
      
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|!(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|b!')
        except (NotParseableException):
            pass
      
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!| ')
        except (NotParseableException):
            pass
  
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|! ')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!| !')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('a!|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('a!|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('a!|!( )')
        except (NotParseableException):
            pass
        
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' |b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator(' |!b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator(' |(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator(' |(!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator(' |!(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' |b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' | ')
        except (NotParseableException):
            pass
    
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' |! ')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' | !')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' |( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' |(! )')
        except (NotParseableException):
            pass
     
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' |!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |!b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |(!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |!(b)')       
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! | ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! | !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('! |!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|!b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|(!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|!(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !| ')
        except (NotParseableException):
            pass


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|! ')
        except (NotParseableException):
            pass


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator(' !|!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|!b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|(!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|!(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )| ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|(! )')
        except (NotParseableException):
            pass

        try:    
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( )|!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|!b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|(!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|!(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )| ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! )|!( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|!b')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|(!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|!(b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|b!')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )| ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|! ')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )| !')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|( )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|(! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('!( )|!( )')
        except (NotParseableException):
            pass









    def test_static_and_parentheses(self):
        

#        evaluator = StringSetEvaluator('(a&b)')

        #+
        evaluator = StringSetEvaluator('(a&b)')
        evaluator = StringSetEvaluator('(a&!b)')    
        evaluator = StringSetEvaluator('(a&(b))')
        evaluator = StringSetEvaluator('(a&(!b))')
        evaluator = StringSetEvaluator('(a&!(b))')

        evaluator = StringSetEvaluator('(!a&b)')
        evaluator = StringSetEvaluator('(!a&!b)')
        evaluator = StringSetEvaluator('(!a&(b))')
        evaluator = StringSetEvaluator('(!a&(!b))')
        evaluator = StringSetEvaluator('(!a&!(b))')

        evaluator = StringSetEvaluator('((a)&b)')
        evaluator = StringSetEvaluator('((a)&!b)')  
        evaluator = StringSetEvaluator('((a)&(b))')
        evaluator = StringSetEvaluator('((a)&(!b))')
        evaluator = StringSetEvaluator('((a)&!(b))')

        evaluator = StringSetEvaluator('((!a)&b)')
        evaluator = StringSetEvaluator('((!a)&!b)')
        evaluator = StringSetEvaluator('((!a)&(b))')
        evaluator = StringSetEvaluator('((!a)&(!b))')
        evaluator = StringSetEvaluator('((!a)&!(b))')

        #-
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a&b!)')
        except (NotParseableException):
            pass

        #TODO not failing
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a& )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a&!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a& )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a&!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)& )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((a)&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)&!( ))')
        except (NotParseableException):
            pass
        
        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)& )')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)&(! ))')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('((!a)&!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&b)')
        except (NotParseableException):
            pass
    
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&!b)') 
        except (NotParseableException):
            pass
       
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&(b))')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&(!b))')
        except (NotParseableException):
            pass
      
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&b!)')
        except (NotParseableException):
            pass
      
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!& )')
        except (NotParseableException):
            pass
  
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&! )')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!& !)')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('(a!&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('(a!&!( ))')
        except (NotParseableException):
            pass
        
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( &b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( &!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( &(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( &(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( &!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( &b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( & )')
        except (NotParseableException):
            pass
    
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( &! )')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( & !)')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( &( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( &(! ))')
        except (NotParseableException):
            pass
     
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( &!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &!(b))')       
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! & )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! & !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! &!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !& )')
        except (NotParseableException):
            pass


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&! )')
        except (NotParseableException):
            pass


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !&!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )& )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&(! ))')
        except (NotParseableException):
            pass

        try:    
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )&!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )& )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )&!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )& )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )& !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )&!( ))')
        except (NotParseableException):
            pass




    def test_static_or_parentheses(self):
        
        #+
        evaluator = StringSetEvaluator('(a|b)')
        evaluator = StringSetEvaluator('(a|!b)')    
        evaluator = StringSetEvaluator('(a|(b))')
        evaluator = StringSetEvaluator('(a|(!b))')
        evaluator = StringSetEvaluator('(a|!(b))')

        evaluator = StringSetEvaluator('(!a|b)')
        evaluator = StringSetEvaluator('(!a|!b)')
        evaluator = StringSetEvaluator('(!a|(b))')
        evaluator = StringSetEvaluator('(!a|(!b))')
        evaluator = StringSetEvaluator('(!a|!(b))')

        evaluator = StringSetEvaluator('((a)|b)')
        evaluator = StringSetEvaluator('((a)|!b)')  
        evaluator = StringSetEvaluator('((a)|(b))')
        evaluator = StringSetEvaluator('((a)|(!b))')
        evaluator = StringSetEvaluator('((a)|!(b))')

        evaluator = StringSetEvaluator('((!a)|b)')
        evaluator = StringSetEvaluator('((!a)|!b)')
        evaluator = StringSetEvaluator('((!a)|(b))')
        evaluator = StringSetEvaluator('((!a)|(!b))')
        evaluator = StringSetEvaluator('((!a)|!(b))')

        #-
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a|b!)')
        except (NotParseableException):
            pass

        #TODO not failing
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a| )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a|!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a| )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!a|!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)| )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((a)|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((a)|!( ))')
        except (NotParseableException):
            pass
        
        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)| )')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):       
                evaluator = StringSetEvaluator('((!a)|(! ))')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('((!a)|!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|b)')
        except (NotParseableException):
            pass
    
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|!b)') 
        except (NotParseableException):
            pass
       
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|(b))')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|(!b))')
        except (NotParseableException):
            pass
      
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|b!)')
        except (NotParseableException):
            pass
      
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!| )')
        except (NotParseableException):
            pass
  
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|! )')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!| !)')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(a!|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('(a!|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('(a!|!( ))')
        except (NotParseableException):
            pass
        
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( |b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( |!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( |(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( |(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):        
                evaluator = StringSetEvaluator('( |!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( |b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( | )')
        except (NotParseableException):
            pass
    
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( |! )')
        except (NotParseableException):
            pass
 
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( | !)')
        except (NotParseableException):
            pass
   
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( |( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( |(! ))')
        except (NotParseableException):
            pass
     
        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( |!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |!(b))')       
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! | )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! | !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(! |!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !| )')
        except (NotParseableException):
            pass


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|! )')
        except (NotParseableException):
            pass


        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('( !|!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )| )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|(! ))')
        except (NotParseableException):
            pass

        try:    
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(( )|!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )| )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('((! )|!( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|!b)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|(!b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|!(b))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|b!)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )| )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|! )')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )| !)')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|( ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|(! ))')
        except (NotParseableException):
            pass

        try:
            with self.assertRaises(NotParseableException):
                evaluator = StringSetEvaluator('(!( )|!( )')
        except (NotParseableException):
            pass


if __name__ == '__main__':
    unittest.main()