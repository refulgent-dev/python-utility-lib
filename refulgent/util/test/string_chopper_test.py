import unittest

from constructa.util.classes.string_chopper import StringChopper

class StringChopperTest(unittest.TestCase):

    def test_single_letter(self):
        
        testStrings = [ 'a',
                        ' a',
                        'a ',
                        ' a ',
                        ' a  ',
                        '  a ',
                        '  a  ']

        compareList = [('va','a')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)

    def test_dual_letter(self):
        
        testStrings = [ 'a a',
                        ' a a',
                        'a a ',
                        ' a a ',
                        ' a a  ',
                        '  a a ',
                        '  a a  ']

        compareList = [('va','a a')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)

    def test_dual_groups2(self):
        
        testStrings = [ 'ab cd',
                        ' ab cd',
                        'ab cd ',
                        ' ab cd ',
                        ' ab cd  ',
                        '  ab cd ',
                        '  ab cd  ']

        compareList = [('va','ab cd')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)

    def test_triple_groups3(self):
        
        testStrings = [ 'xyz 123 456',
                        ' xyz 123 456',
                        'xyz 123 456 ',
                        ' xyz 123 456 ',
                        ' xyz 123 456  ',
                        '  xyz 123 456 ',
                        '  xyz 123 456  ']

        compareList = [('va','xyz 123 456')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)

    def test_triple_groups3(self):
        
        testStrings = [ 'xyz 123 456',
                        ' xyz 123 456',
                        'xyz 123 456 ',
                        ' xyz 123 456 ',
                        ' xyz 123 456  ',
                        '  xyz 123 456 ',
                        '  xyz 123 456  ']

        compareList = [('va','xyz 123 456')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)

    def test_letter2_and_letter2(self):
        
        testStrings = [ 'ab&12',
                        ' ab&12',
                        'ab&12 ',
                        '  ab&12',
                        'ab&12  ',
                        ' ab&12  ',
                        ' ab&12  ',
                        '  ab&12  ',
                        'ab &12',
                        ' ab &12',
                        'ab &12 ',
                        '  ab &12',
                        'ab &12  ',
                        ' ab &12  ',
                        ' ab &12  ',
                        '  ab &12  ',
                        'ab& 12',
                        ' ab& 12',
                        'ab& 12 ',
                        '  ab& 12',
                        'ab& 12  ',
                        ' ab& 12  ',
                        ' ab& 12  ',
                        '  ab& 12  ',
                        'ab & 12',
                        ' ab & 12',
                        'ab & 12 ',
                        '  ab & 12',
                        'ab & 12  ',
                        ' ab & 12  ',
                        ' ab & 12  ',
                        '  ab & 12  ',
                        'ab  &12',
                        ' ab  &12',
                        'ab  &12 ',
                        '  ab  &12',
                        'ab  &12  ',
                        ' ab  &12  ',
                        ' ab  &12  ',
                        '  ab  &12  ',
                        'ab&  12',
                        ' ab&  12',
                        'ab&  12 ',
                        '  ab&  12',
                        'ab&  12  ',
                        ' ab&  12  ',
                        ' ab&  12  ',
                        '  ab&  12  ',
                        'ab &  12',
                        ' ab &  12',
                        'ab &  12 ',
                        '  ab &  12',
                        'ab &  12  ',
                        ' ab &  12  ',
                        ' ab &  12  ',
                        '  ab &  12  ',
                        'ab  & 12',
                        ' ab  & 12',
                        'ab  & 12 ',
                        '  ab  & 12',
                        'ab  & 12  ',
                        ' ab  & 12  ',
                        ' ab  & 12  ',
                        '  ab  & 12  ',
                        'ab  &  12',
                        ' ab  &  12',
                        'ab  &  12 ',
                        '  ab  &  12',
                        'ab  &  12  ',
                        ' ab  &  12  ',
                        ' ab  &  12  ',
                        '  ab  &  12  ']

        compareList = [('va', 'ab'), ('op', '&'), ('va', '12')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(compareList, tokenList)


    def test_letter2_or_letter2(self):
        
        testStrings = [ '34|mn',
                        ' 34|mn',
                        '34|mn ',
                        '  34|mn',
                        '34|mn  ',
                        ' 34|mn  ',
                        ' 34|mn  ',
                        '  34|mn  ',
                        '34 |mn',
                        ' 34 |mn',
                        '34 |mn ',
                        '  34 |mn',
                        '34 |mn  ',
                        ' 34 |mn  ',
                        ' 34 |mn  ',
                        '  34 |mn  ',
                        '34| mn',
                        ' 34| mn',
                        '34| mn ',
                        '  34| mn',
                        '34| mn  ',
                        ' 34| mn  ',
                        ' 34| mn  ',
                        '  34| mn  ',
                        '34 | mn',
                        ' 34 | mn',
                        '34 | mn ',
                        '  34 | mn',
                        '34 | mn  ',
                        ' 34 | mn  ',
                        ' 34 | mn  ',
                        '  34 | mn  ',
                        '34  |mn',
                        ' 34  |mn',
                        '34  |mn ',
                        '  34  |mn',
                        '34  |mn  ',
                        ' 34  |mn  ',
                        ' 34  |mn  ',
                        '  34  |mn  ',
                        '34|  mn',
                        ' 34|  mn',
                        '34|  mn ',
                        '  34|  mn',
                        '34|  mn  ',
                        ' 34|  mn  ',
                        ' 34|  mn  ',
                        '  34|  mn  ',
                        '34 |  mn',
                        ' 34 |  mn',
                        '34 |  mn ',
                        '  34 |  mn',
                        '34 |  mn  ',
                        ' 34 |  mn  ',
                        ' 34 |  mn  ',
                        '  34 |  mn  ',
                        '34  | mn',
                        ' 34  | mn',
                        '34  | mn ',
                        '  34  | mn',
                        '34  | mn  ',
                        ' 34  | mn  ',
                        ' 34  | mn  ',
                        '  34  | mn  ',
                        '34  |  mn',
                        ' 34  |  mn',
                        '34  |  mn ',
                        '  34  |  mn',
                        '34  |  mn  ',
                        ' 34  |  mn  ',
                        ' 34  |  mn  ',
                        '  34  |  mn  ']

        compareList = [('va', '34'), ('op', '|'), ('va', 'mn')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)


    def test_not_letter2_or_letter2(self):
        
        testStrings = [ '!AB|_X',
                        ' !AB|_X',
                        '!AB|_X ',
                        '  !AB|_X',
                        '!AB|_X  ',
                        ' !AB|_X  ',
                        ' !AB|_X  ',
                        '  !AB|_X  ',
                        '!AB |_X',
                        ' !AB |_X',
                        '!AB |_X ',
                        '  !AB |_X',
                        '!AB |_X  ',
                        ' !AB |_X  ',
                        ' !AB |_X  ',
                        '  !AB |_X  ',
                        '!AB| _X',
                        ' !AB| _X',
                        '!AB| _X ',
                        '  !AB| _X',
                        '!AB| _X  ',
                        ' !AB| _X  ',
                        ' !AB| _X  ',
                        '  !AB| _X  ',
                        '!AB | _X',
                        ' !AB | _X',
                        '!AB | _X ',
                        '  !AB | _X',
                        '!AB | _X  ',
                        ' !AB | _X  ',
                        ' !AB | _X  ',
                        '  !AB | _X  ',
                        '!AB  |_X',
                        ' !AB  |_X',
                        '!AB  |_X ',
                        '  !AB  |_X',
                        '!AB  |_X  ',
                        ' !AB  |_X  ',
                        ' !AB  |_X  ',
                        '  !AB  |_X  ',
                        '!AB|  _X',
                        ' !AB|  _X',
                        '!AB|  _X ',
                        '  !AB|  _X',
                        '!AB|  _X  ',
                        ' !AB|  _X  ',
                        ' !AB|  _X  ',
                        '  !AB|  _X  ',
                        '!AB |  _X',
                        ' !AB |  _X',
                        '!AB |  _X ',
                        '  !AB |  _X',
                        '!AB |  _X  ',
                        ' !AB |  _X  ',
                        ' !AB |  _X  ',
                        '  !AB |  _X  ',
                        '!AB  | _X',
                        ' !AB  | _X',
                        '!AB  | _X ',
                        '  !AB  | _X',
                        '!AB  | _X  ',
                        ' !AB  | _X  ',
                        ' !AB  | _X  ',
                        '  !AB  | _X  ',
                        '!AB  |  _X',
                        ' !AB  |  _X',
                        '!AB  |  _X ',
                        '  !AB  |  _X',
                        '!AB  |  _X  ',
                        ' !AB  |  _X  ',
                        ' !AB  |  _X  ',
                        '  !AB  |  _X  ']

        compareList = [('va', '!AB'), ('op', '|'), ('va', '_X')]

        for t in testStrings:
             tokenList = StringChopper(t).getTokenList()
             self.assertEqual(tokenList, compareList)


if __name__ == '__main__':
    unittest.main()

