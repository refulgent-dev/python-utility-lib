#!/usr/bin/env python3

# StringSetEvaluator v0.1.1
#
# -------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <javanaut@refulgent.de> wrote this file. As long as you retain this 
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return
# -------------------------------------------------------------------------
#
# Purpose: 
#   Tests a query list of strings against a complex test expression
#
# Usage:
#
# Tested with:
#   Python 3.8.2 64-bit on CentOS 8 64-bit
#
# Hint:

import os
import sys
import hashlib

from constructa.util.classes.not_parseable_exception import NotParseableException
from constructa.util.classes import Tokenizer

class StringSetEvaluator():

    def __init__(self, nodes, operator=None, depth = 0, count=0):

        self.__debugLevel = 0

        StringSetEvaluator.__instanceCount = count
        self.__order = count

        self.__depth = depth

        if self.__depth == 99:
            raise NotParseableException(self, 'Tree iteration ran into nirvana')


        if (nodes == None or not nodes) and not nodes == '':
            raise NotParseableException(self, 'Allowed input for nodes is: str, [tuple] and [StringSetEvaluator]')

        teststring = nodes

        # use auxillary class to prepare token list from string
        if type(nodes) is str:
            nodes = Tokenizer(nodes).tokenize()

        # essential attributes
        self._operator = operator
        self._nodes = nodes
        self._value = None
        
        if (not self._operator and # Tx
                len(self._nodes) == 1 and 
                self.__isParameterToken(self._nodes[0])):

            self._operator = 'X'
            self._value = self._nodes[0][1]
            self._nodes = None # Delete list to prevent invalid processing

        ## Parsing

        if self._nodes:

            self.__parseParentheses()
            self.__parseNeg()
            self.__parseAndOr('&')
            self.__parseAndOr('|')
            self.__parseValue()

        if self.__depth == 0:
            self._cleanTree()

        if self.__depth == 0:
            self.validate()

    # Parsing methods
 
    def __parseParentheses(self):

        lPtr = 0
        rPtr = 0
        pHeight = 0

        order = self.__order

        if self.__debugLevel > 1:
            print('%02i: {%i} checking for parentheses: <%s>' % (self.__order, self.__depth,
                        self.nodesToString(self._nodes)))

            if self.__debugLevel > 2:
                for node in self._nodes:
                    print('N: %s' % str(node))

        for node in self._nodes:

            tokenOperator = self.__getTokenOperator(node)
            
            if tokenOperator == '(':
                if pHeight == 0:
                    lPtr = rPtr
                pHeight += 1
                
            if tokenOperator == ')':
                pHeight -= 1
                if pHeight == 0:

                    leftResidue = self._nodes[:lPtr]
                    capture = self._nodes[lPtr+1:rPtr]
                    rightResidue = self._nodes[rPtr+1:]

                    if self.__debugLevel > 1:
                        print('%02i: {%i} parentheses captured: {%i} {%i} {%i} <%s> <%s> <%s>' % (
                                    self.__order, self.__depth, 
                                    len(leftResidue),len(capture),len(rightResidue),
                                    self.nodesToString(leftResidue), self.nodesToString(capture), self.nodesToString(rightResidue)))

                    nodeBuffer = list()

                    if leftResidue:
                        nodeBuffer += leftResidue

                    if capture:
                        nodeBuffer.append(StringSetEvaluator(
                                capture, depth=self.__depth + 1,
                                count = StringSetEvaluator.__instanceCount + 1))
                    else:
                        raise NotParseableException(self, "Empty parentheses are invalid!")

                    if rightResidue:
                        nodeBuffer += rightResidue

                    if self.__debugLevel > 1:
                        print('%02i: {%i} returning nodesBuffer: %s' % (self.__order, self.__depth, self.nodesToString(nodeBuffer)))

                        for node in nodeBuffer:
                            print('    %s' % str(node))
                        for node in self._nodes:
                            print('!    %s' % str(node))

                    self._nodes = nodeBuffer

                    self.__parseParentheses()
                    return

            rPtr += 1

        if self.__debugLevel > 1:
            print('checking done')

        if pHeight != 0:
            raise NotParseableException(self, 'Invalid parentheses at position %i (depth = %i): %s' % (lPtr , self.__depth, str(self._nodes)))

    # T!X mit X = Tx,Nx,Nt,N!,N&,N| isParseable für diese Klassen   
    def __parseNeg(self):

        if len(self._nodes) < 2: # cannot be any TxT&Tx or similar
            return 

        nodeBuffer = list()
        prevNode = None
        wasActive = False

        for index, node in enumerate(self._nodes):

            if (self.__getTokenOperator(node) == '!' and # T! at end
                    index == len(self._nodes) - 1):
                raise NotParseableException(self, 'Negation at end of test expression')

            if (self.__getTokenOperator(prevNode) == '!' and # !T&, !T|, !T&
                    not self.__getTokenOperator(node)):
                    
                nodeBuffer.pop()
                nodeBuffer.append(StringSetEvaluator([node], operator='!',
                            depth=self.__depth + 1, # N!
                            count=StringSetEvaluator.__instanceCount + 1)) 
                wasActive = True
            else:
                nodeBuffer.append(node)

            prevNode = node
            
            
        self._nodes = nodeBuffer        

        if wasActive:
            self.__parseNeg()

    # XT&X mit X = Tx,Nx,Nt, N&, N|
    def __parseAndOr(self, operator):

        if len(self._nodes) < 3: # cannot be any TxT&Tx or similar
            return 

        nodeBuffer = list()
        prevNode = None
        prevPrevNode = None
        wasActive = False

        for node in self._nodes:

            if self.__debugLevel > 1:
                print('N: %s P: %s PP: %s' % (type(node), type(prevNode), type(prevPrevNode))) 

            if (self.__getTokenOperator(prevNode) == operator and
                    not self.__getTokenOperator(node) and
                    not self.__getTokenOperator(prevPrevNode)):

                nodeBuffer.pop()
                nodeBuffer.pop()
                nodeBuffer.append(StringSetEvaluator([prevPrevNode, node],
                            operator=operator, depth=self.__depth + 1,
                            count=StringSetEvaluator.__instanceCount + 1))
                wasActive = True
            else:
                nodeBuffer.append(node)


            prevPrevNode = prevNode
            prevNode = node

        self._nodes = nodeBuffer

        if wasActive:
            self.__parseAndOr(operator)

    def __parseValue(self):

        nodeBuffer = list()

        for node in self._nodes:

            if self.__isParameterToken(node): # Tx

                if self.__debugLevel > 0:
                    print('%02i: {%i} creating value node (Nx)' % (self.__order, self.__depth))

                nodeBuffer.append(StringSetEvaluator([node], # Nx
                            depth=self.__depth + 1,
                            count=StringSetEvaluator.__instanceCount + 1)) 
            else:
                nodeBuffer.append(node)
               
        self._nodes = nodeBuffer        


    def _cleanTree(self):

        if self.__debugLevel > 0:
            print('cleanTree() %s' % str(self))

        if self._nodes: # !Nx
            for node in self._nodes:
                if type(node) is self.__class__:
                    node._cleanTree()

            if len(self._nodes) == 1: # Fuseable
                if (type(self._nodes[0]) is self.__class__ and 
                        not self._operator):
                    
                    self._operator = self._nodes[0]._operator
                    self._value = self._nodes[0]._value     
                    self._nodes = self._nodes[0]._nodes

    def __getTokenOperator(self, token):

        if type(token) is tuple and token[0] == 'op':
            return token[1]

        return None

    def __isParameterToken(self, token):
        return type(token) is tuple and token[0] == 'va'


    # Evaluation method

    def evaluate(self, queryList):

        # Empty query list shall be equivalent to empty string here
        if queryList == None or not queryList:
            queryList = ['']

        if self._operator == 'X':
            return self._value in queryList

        if self._operator == '|':
            return self._nodes[0].evaluate(queryList) or self._nodes[1].evaluate(queryList)

        if self._operator == '&':
            return self._nodes[0].evaluate(queryList) and self._nodes[1].evaluate(queryList)

        if self._operator == '!':
            return not self._nodes[0].evaluate(queryList)

        if not self._operator:
            return self._nodes[0].evaluate(queryList)

        raise NotParseableException(self, 'Illegal operator!')



    def validate(self):

        if not self._operator:
            raise NotParseableException(self,'Operator is None')

        if self._operator == 'X' and self._value is None:
            raise NotParseableException(self, 'Value is Node')

        if self._operator == '!' and not len(self._nodes) == 1:
            raise NotParseableException(self, 'Missing parameter in negation')

        if self._operator == '|' and not len(self._nodes) == 2:
            raise NotParseableException(self, 'Missing parameter in or')

        if self._operator == '&' and not len(self._nodes) == 2:
            raise NotParseableException(self, 'Missing parameter in and')

        if self._nodes:
            for node in self._nodes:
                node.validate()
        
   
        



    # Helper methods

    def nodesToString(self, nodeList):

        childString = ''

        if nodeList: # !Nx

            for node in nodeList:
                if type(node) is self.__class__:
                    childString += '%02i ' % node.__order
                if type(node) is tuple:
                    if node[0] == 'op':
                        childString += 'T%s ' % node[1]
                    if node[0] == 'va':
                        childString += 'TX '

        return childString.strip()

    def shash(self, obj):
        return hashlib.sha3_512(repr(obj).encode('utf-8')).hexdigest()[:4]

    def __str__(self):

        return '%02i: [%s] {%i} (%s) <%s> %s' % (self.__order,
                                        self._operator,
                                        self.__depth,
                                        self.shash(self),
                                        self.nodesToString(self._nodes) if self._nodes else '',
                                        (self._value if self._value else ''))        

    def printNodeList(self):

        spacer = ''
        for index in range(self.__depth):
            spacer += '  '

        print (spacer + str(self))

        if self._nodes: #!Nx
            for node in self._nodes:
                if type(node) is self.__class__:
                    node.printNodeList()