from setuptools import setup

setup(
    name='python-utility-lib',
    version='0.2.0',
    description='Miscellaneous tools and classes for python',
    url='git@bitbucket.org:refulgent-dev/python-utility-lib.git',
    author='Javanaut',
    author_email='javanaut@refulgent.de',
    license='unlicense',
    packages=['refulgent.util','refulgent.simplespawner'],
    zip_safe=False
)